#include <iostream>
#include <iomanip>

using namespace std;
void showBalance(double balance);
double withdraw(double balance);
double deposit();

int main(){
    int balance = 450.02;
    string Account_Holder = "Austin Alan Hill";
    int choice;
    do{
    cout << "welcome " << Account_Holder << '\n';
    cout << "**********************************\n";
    cout << "what would you like to do?\n"; 
    cout << "**********************************\n";

     
    cout << "1. Show Balance\n";
    cout << "2. Withdraw\n";
    cout << "3. deposit\n";
    cout << "4. exit\n";
   
    cin >> choice;

        switch(choice){
            case 1:
                showBalance(balance);
                break;
            case 2:
                balance -= withdraw(balance);
                showBalance(balance);
                break;
            case 3:
                balance += deposit();
                showBalance(balance);
                break;
            case 4:
                cout << "Good bye " << Account_Holder << endl;
                break;
            default:
                cout << "invlaid choice" << endl;
                
        }

    }
    while(choice != 4);

    return 0;
}
void showBalance(double balance){
    cout << "Your balance is: $" << setprecision(2) << fixed << balance << endl;
}
double withdraw(double balance){
    double amount = 0;
    
    cout << "Enter amount to withdraw: ";
    cin >> amount;

    if(amount > balance){
        cout << "insufficent funds";
        return 0;
    }
    return amount;
    
}
double deposit(){
    double amount = 0;
    cout << "Enter amount to be deposited: ";
    cin >> amount;
    if(amount < 0)
        cout << "invalid " << endl;
    else 
    return amount;
}







   